FROM debian:buster-slim

# create a system groud named bitcoin
# create new user (-r = system account), (-m create users home directory if it does not exist), (-g group name to which we add new user)
RUN groupadd -r bitcoin && useradd -r -m -g bitcoin bitcoin

# set -ex -> set sets or unsets values of shell options and positional parameters (-e = Exit imediately if command return non 0), (-x print commands and their arguments as they are executed)
# update package list and install and then remove some unnecessary files...
RUN set -ex \
	&& apt-get update \
	&& apt-get install -qq --no-install-recommends ca-certificates dirmngr gosu gpg wget \
	&& rm -rf /var/lib/apt/lists/*


# set some environamnet variables
ENV BITCOIN_VERSION 1.0.6
ENV BITCOIN_URL https://bitbucket.org/zkokelj/test123/raw/6e2dc77bda4f986ba6ff3a5e7b4496bb6600f8c7/bitcoin.tar.gz
# ENV BITCOIN_SHA256 4dd979080c446c79bb577fc8e3a3babf83b76641a3e3c4268e8abcefdcb767d4

# install bitcoin binaries
# tar (-v = verbose) (-z= filter through gzip) (-xf= Extract all files from archive.tar!) 
#  -C (directory to where we want to extract -> /usr/local)
# remove .tar.gz file
RUN set -ex \
	&& cd /tmp \
	&& wget -qO bitcoin.tar.gz "$BITCOIN_URL" \
	# && echo "$BITCOIN_SHA256 bitcoin.tar.gz" | sha256sum -c - \
	&& tar -xzvf bitcoin.tar.gz -C /usr/local --strip-components=1 --exclude=*-qt \
	&& rm -rf /tmp/*

# create data directory, give it to the right owner
# ln -sfn (make links between files: -s = symbolic, -f = force, -n = no-derefernece)
# Make a symolic link and change owhership of  
ENV BITCOIN_DATA /data
RUN mkdir "$BITCOIN_DATA" \
	&& chown -R bitcoin:bitcoin "$BITCOIN_DATA" \
	&& ln -sfn "$BITCOIN_DATA" /home/bitcoin/.bitcoin \
	&& chown -h bitcoin:bitcoin /home/bitcoin/.bitcoin

# Create a volume (creates a mount point with the specified name and marks it as holding externally mounted volumes from host or other containers)
VOLUME /data

# Copy script in the container
COPY docker-entrypoint.sh /entrypoint.sh

# 2 forms (exec (better & used below) and shell)
# command line arguments to docker run will be appended after all elements in an exec form ENTRYPOINT (& will override all elements specified using CMD).
# You can override entrypoint with : `docker run --entrypoint`
ENTRYPOINT ["/entrypoint.sh"]

#Expose informs docker that the container listens on the specified network ports at runtime
EXPOSE 8332 8333 9332 9333 18332 18333

RUN mkdir /cores

# CMD can have three forms:
# -> exec form (this one below!) - preferred form
# -> ["param1", "param2"] - as default parameters to ENTRYPOINT
# -> shell form (same effect as exec, but not recommended)
# There can be only ONE CMD instruction in Dockerfile (only the last one will have effect)
# The main purpose of a CMD is to provide defaults for an executing container

CMD ["bitcoind", "-excessiveblocksize=0", "-maxstackmemoryusageconsensus=0"]
